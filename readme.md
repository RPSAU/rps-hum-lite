

## Allow a user to have rw access to /dev/spi* 
## create spi group
sudo -g 10000 spi
### add user to spi group
sudo adduser ubuntu spi
### set permssion to /dev/spi*
sudo chown root:spi /dev/spi*
sudo chmod 660 /dev/spi*
