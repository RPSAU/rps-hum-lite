from flask import Flask, jsonify, request, Response
import time
import datetime
import json
import logging
import requests
from libs.device import devices
import libs.utils as utils
import os
import sys

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

# define api service at port 8000. To be add in the station config file
api_version = 'v1.0'
HOST = "0.0.0.0"
PORT = 8000
api_app = Flask(__name__)

# ==========================================
# define common functions
# ==========================================

# load the config defined by fn


def load_config(fn):
    conf = None
    with open(os.path.join(APP_PATH, "configs", fn), "r") as f:
        conf = json.load(f)
    return conf

# save the config defined by fn


def save_config(fn):
    with open(os.path.join(APP_PATH, "configs", fn), "w") as f:
        json.dump(stationConf, f, indent=4)

# save station config


def save_stationConf():
    with open(os.path.join(APP_PATH, "configs/stationconf.json"), "w") as f:
        json.dump(stationConf, f, indent=4)

# ===========================================================
# define the configuration and support functions for apiService
# ===========================================================


@api_app.route('/')
def index():
    # read hs315
    hs315_data = devs["hs315"].read()
    #hs315_data = {}
    solar_data = devs["solar_mppt"].read()
    #solar_data = {}
    shunt_data = devs["ve_shunt"].read()
    #shunt_data = {}
    global stationConf
    global gpsLastUpdate
    global gps_data
    if gpsLastUpdate == None or gps_data == "":
        gps_data = devs["sim7600sa"].readGPSdata()
        gpsLastUpdate = datetime.datetime.now()
    elif (datetime.datetime.now() - gpsLastUpdate).total_seconds() > 60*10:
        gps_data = devs["sim7600sa"].readGPSdata()
        gpsLastUpdate = datetime.datetime.now()
    else:
        print("no update GPS data")
    
    data = {
        "info_sys": {
            "sn": stationConf["sn"],
            "timestamp_utc": datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
            },
        "info_gps": gps_data,
        "info_gen": hs315_data,
        "info_mppt": solar_data,
	"info_veshunt": shunt_data
    }
                    
    return json.dumps(data)

# ==============================================
# Define entry point of processes
# ==============================================

# Define the entry point for apiService
def apiApp():
    api_app.run(host=HOST, port=PORT)

# Define the entry point for Scheduler

if __name__ == '__main__':

    APP_PATH = os.path.dirname(os.path.abspath(__file__))
    logging.basicConfig(format="%(asctime)s:%(levelname)s:%(message)s", filename=os.path.join(
        APP_PATH, "logs/rpshum.log"), level=logging.DEBUG)

    # load station config
    stationConf = load_config("stationconf.json")
    stationDevicesConf = stationConf["devices"]
    
    gps_data = ""
    gpsLastUpdate = None
    
    # intialize devices.
    devs = devices.Devices()
    for devId in stationDevicesConf.keys():
        devConf = stationDevicesConf[devId]
        if devConf["isEnabled"]:
            print("add device {} ({})".format(devConf["name"], devId))
            devs.add_device(devId, devConf)

    print("{} device(s), {} active".format(
        len(stationDevicesConf), len(devs.DeviceList())))
    
    #print(devs["hs315"].read())
    #print(devs["solar_mppt"].read())
    #for i in range(1):
        #print(devs["ve_shunt"].read())
    #print(devs["sim7600sa"].readGPSdata())
    
    
    apiApp()

