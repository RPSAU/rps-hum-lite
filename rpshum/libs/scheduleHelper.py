import libs.utils as utils

def getJobIds(devId, contId, cont):
    jobIds = []
    # if cont["type"].upper() == "TIME":
        # jobId = "{}_{}".format(devId, contId)
        # jobIds.append(jobId)
    # elif cont["type"].upper() == "DAY":
        # pass
    # else:
        # pass
    jobId = "{}_{}".format(devId, contId)
    jobIds.append(jobId)
    return jobIds

def scheduleTimeJob(sch, conf):
	jobs = {}
	return jobs

def scheduleSensorJob(sch, devconf):
	return jobs

def scheduleJob(sch, conf):
	if conf["type"] == "TIME":
		job = scheduleTimeJob(job,conf)
	elif conf["type"] == "DEVICE":
		job = scheduleSensorJob(job,conf)

	return job
'''
def scheduleDeviceJobs(sch, devId, contingents):
	jobs = {}	
	
	return jobs		
'''

def runTimeAtJob(cont, devId, devs):

    pass

def checkSenseCondition(cont,devs):
    for sensecont in cont["senseDevices"]:
        dev = devs[sensecont["deviceId"]]
        val = dev.read_measurement(sensecont["measurement"])
        print("read PIR val {}".format(val))
        if not utils.checkSenseCondition(val,sensecont):
            return False
    return True    

def runTimeBetweenJob():
    print("Running time between job ")
    daysofweek = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
    while True:
        cur_dt =  datetime.datetime.now()
        cur_day = daysofweek[cur_dt.weekday()]
        # break if it is not on the pickDays
        if cont["pickDays"][cur_day] == False:
            break
        
        # break if it is not in timeRange
        if utils.checkTimeCondition(cont["timeRangeStart"], cont["timeRangeEnd"]) == False:
            break
        
        # check device sensing
        if len(cont["senseDevices"]) == 0 :
            #self.__setContingent(cont["onContingent"])
            time.sleep(cont["senseRate"])
            continue
        
        if devs is None:
            print("No device list for sense contingent")
            #time.sleep(cont["senseRate"])
            break;
        
        '''
        isSense = True
        for sensecont in cont["senseDevices"]:
            dev = devs[sensecont["deviceId"]]
            val = dev.read_measurement(sensecont["measurement"])
            print("sense {}: {}".format(sensecont["measurement"], val))
            if val is None:
                isSense = False
                break
            
            if not utils.checkSenseCondition(val,sensecont):
                isSense = False
                break
        '''        
        #if isSense:
        if checkSenseCondition(cont,devs):
            self.__setContingent(cont["onContingent"])
            if cont["onContingent"] == True:
                if "holdTime" in cont.keys():
                    if cont["holdTime"] > 0:
                        time.sleep(cont["holdTime"])
                        self.__setContingent(False)
                    
        time.sleep(cont["senseRate"])    
    pass

def runJob(contId, cont, devId, devs):
    if devId not in devs.DeviceList().keys():
        print("Device not found for running contingent {}".format(cont))
        return
    
    dev  = devs[devId]
    
    if cont["timeRange"].upper() == "AT":
        runTimeAtJob()
    elif cont["timeRange"].upper() == "BETWEEN":
        runtTimeBetweenJob()