# Author: Jacob Hartland
# Company: Elemental Electronics
# Description: Basic library for ADS1115
# Functionality: Provides read and write commands for ADS1115.

import smbus
import time

#https://www.ti.com/product/ADS1115

# Register and other configuration values:
ADS1115_DEFAULT_ADDRESS         = 0x48

ADS1115_POINTER_CONVERSION      = 0x00,2
ADS1115_POINTER_CONFIG          = 0x01,2
ADS1115_POINTER_LOW_THRESHOLD   = 0x02,2
ADS1115_POINTER_HIGH_THRESHOLD  = 0x03,2

ADS1115_CONFIG_CH0_SINGLE       = 0xC383
ADS1115_CONFIG_CH1_SINGLE       = 0xD383
ADS1115_CONFIG_CH2_SINGLE       = 0xE383
ADS1115_CONFIG_CH3_SINGLE       = 0xF383

CH_SINGLE = [ADS1115_CONFIG_CH0_SINGLE,ADS1115_CONFIG_CH1_SINGLE,ADS1115_CONFIG_CH2_SINGLE,ADS1115_CONFIG_CH3_SINGLE]

class ADS1115(object):
    """Base functionality for ADS1115"""

    def __init__(self, bus, addr=ADS1115_DEFAULT_ADDRESS, **kwargs):

        self.bus = bus
        self.addr = addr
        self.busId = 0

    def read(self, reg=ADS1115_POINTER_CONVERSION, debug=0):
        data = None
        try:
            #self.bus.open(self.busId)
            data = self.bus.read_i2c_block_data(self.addr, reg[0], reg[1])
            if debug == 1:
                print("I2C RECIEVED: " + " ".join("\\x%02x" % i for i in data))
            #self.bus.close()
        except Exception as e:
            #self.bus.close()
            print(e)

        return data

    def write(self, data, reg=ADS1115_POINTER_CONFIG):
        try:
            #self.bus.open(self.busId)
            sendarray = bytearray.fromhex('{:0004x}'.format(data))
            bits = [int(sendarray[0]) , int(sendarray[1])]
            self.bus.write_i2c_block_data(self.addr, reg[0], bits)
            #self.bus.close()
        except Exception as e:
            #self.bus.close()
            print(e)

    def twos_complement_conversion(self, low, high):
        # Convert to 16-bit signed value.
        value = ((high & 0xFF) << 8) | (low & 0xFF)
        # Check for sign bit and turn into a negative value if set.
        if value & 0x8000 != 0:
            value -= 1 << 16
        #From ADS1115 datasheet FSR (+/-)4.096V = LSB 125uV
        value = value * 0.000125
        #From PCB Schematic Vin_max = 28V | Vout_max = 3.134V
        value = value * (28/3.134)

        return value

    def read_adc(self, ch):
        '''
        if ch == 0:
            self.write(ADS1115_CONFIG_CH0_SINGLE)
        elif ch == 1:
            self.write(ADS1115_CONFIG_CH1_SINGLE)
        elif ch == 2:
            self.write(ADS1115_CONFIG_CH2_SINGLE) 
        elif ch == 3:
            self.write(ADS1115_CONFIG_CH3_SINGLE)
        '''
        data = None
        try:
            self.bus.open(self.busId)
            if ch >= 0 and ch <= 3:
                self.write(CH_SINGLE[ch])
        
            #sample time delay
            time.sleep(0.010)

            data = self.read()
            #self.bus.open(self.busId)
            self.bus.close()
            data = self.twos_complement_conversion(data[1],data[0])
            
        except Exception as e:
            print(e)
            self.bus.close()
        return data
