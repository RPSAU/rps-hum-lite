# Author: Jacob Hartland
# Company: Elemental Electronics
# Description: Basic library for TMP1075
# Functionality: Provides read and write commands for TMP1075.

import smbus
import time

#https://www.ti.com/product/TMP1075

# Register and other configuration values:
TMP1075_DEFAULT_ADDRESS             = 0x4F

TMP1075_REGISTER_TEMPERATURE        = 0x00, 2

class TMP1075(object):
    """Base functionality for TMP1075"""

    def __init__(self, bus, addr=TMP1075_DEFAULT_ADDRESS, **kwargs):

        self.bus = bus
        self.addr = addr
        self.busId = 0

    def read(self, reg=TMP1075_REGISTER_TEMPERATURE, debug=0):

        data = self.bus.read_i2c_block_data(self.addr, reg[0], reg[1])
    
        if debug == 1:
            print("I2C RECIEVED: " + " ".join("\\x%02x" % i for i in data))

        return data

    def read_temperature(self):
        data = None
        try:
            self.bus.open(self.busId)
            values = self.read()
            data = self.twos_complement_conversion(values[1],values[0])
            self.bus.close()
        except Exception as e:
            print(e)
            self.bus.close()
        
        return data
        
    def twos_complement_conversion(self, low, high):
        # Convert to 12-bit signed value.
        value = ((high & 0xFF) << 4) | ((low & 0xFF) >> 4)
        # Check for sign bit and turn into a negative value if set.
        if value & 0x800 != 0:
            value -= 1 << 12
        #From TMP1075 datasheet each bit = 0.0625C
        value = value * 0.0625

        return value