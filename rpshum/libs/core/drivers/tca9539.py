# Author: Jacob Hartland
# Company: Elemental Electronics
# Description: Basic library for TCA9539
# Functionality: Provides read and write commands for TCA9539.

import smbus
import time

#https://www.ti.com/product/TCA9539

# Register and other configuration values:
TCA9539_DEFAULT_ADDRESS             = 0x74

TCA9539_REGISTER_INPUT_PORT0        = 0x00, 1
TCA9539_REGISTER_INPUT_PORT1        = 0x01, 1
TCA9539_REGISTER_OUTPUT_PORT0       = 0x02, 1
TCA9539_REGISTER_OUTPUT_PORT1       = 0x03, 1
TCA9539_REGISTER_POLARITY_PORT0     = 0x04, 1
TCA9539_REGISTER_POLARITY_PORT1     = 0x05, 1
TCA9539_REGISTER_CONFIG_PORT0       = 0x06, 1
TCA9539_REGISTER_CONFIG_PORT1       = 0x07, 1

class TCA9539(object):
    """Base functionality for TCA9539"""

    def __init__(self, bus, addr=TCA9539_DEFAULT_ADDRESS, **kwargs):

        self.bus = bus
        self.addr = addr
        self.busId = 0
		
        # initialize the digital ports
        self.write(0x00, TCA9539_REGISTER_CONFIG_PORT1)
        self.write(0x00)

    def read(self, reg=TCA9539_REGISTER_INPUT_PORT0, debug=0):
        data = None
        try:
            self.bus.open(self.busId)
            data = self.bus.read_i2c_block_data(self.addr, reg[0], reg[1])
            if debug == 1:
                print("I2C RECEIVED: " + " ".join("\\x%02x" % i for i in data))
            self.bus.close()
        except Exception as e:
            print(e)
            self.bus.close()
            
        return data

    def write(self, data, reg=TCA9539_REGISTER_OUTPUT_PORT1):
        try:
            self.bus.open(self.busId)
            self.bus.write_byte_data(self.addr, reg[0], data)
            self.bus.close()
        except Exception as e:
            print(e)
            self.bus.close()
