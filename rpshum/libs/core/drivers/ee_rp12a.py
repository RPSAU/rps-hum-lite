# Author: Jacob Hartland
# Company: Elemental Electronics
# Description: Basic library for RP12A
# Functionality: Provides pinouts and interfaces for RP12A.

import spidev
import smbus
import RPi.GPIO as GPIO
import serial
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#PCB Pinouts for CM3+
class pinout:
    WDT_PULSE_LV = 0
    PHY_RESET_N_HV = 1
    SPARE_NU0 = 2
    SPARE_NU1 = 3
    SPARE_NU2 = 4 
    SPARE_NU3 = 5
    SPI0_CS2 = 6
    SPI0_CS1 = 7
    SPI0_CS0 = 8
    SPI0_MISO = 9
    SPI0_MOSI = 10
    SPI0_SCK = 11
    AUD_PWM0 = 12
    AUD_PWM1 = 13
    UART0_TXD = 14
    UART0_RXD = 15
    UART0_CTS = 16
    UART0_RTS = 17
    SIM_nRESET = 18
    STATUS_LED = 19
    SPARE_F0 = 20
    SPARE_F1 = 21
    SPARE_F2 = 22
    SPARE_F3 = 23
    BT_EN = 24
    UART1B_HOST_WAKE = 25
    UART1B_DEV_WAKE = 26
    UART1B_REG_ON = 27
    I2C0_SDA = 28
    I2C0_SCL = 29
    UART1_CTS = 30
    UART1_RTS = 31
    UART1_TXD = 32
    UART1_RXD = 33
    SDIO1_CLK = 34
    SDIO1_CMD = 35
    SDIO1_DAT0 = 36
    SDIO1_DAT1 = 37
    SDIO1_DAT2 = 38
    SDIO1_DAT3 = 39
    SDIO1_DEV_WAKE = 40
    SDIO1_HOST_WAKE = 41
    SDIO1_REG_ON = 42
    WDT_EN_LV = 43
    I2C1_SDA = 44
    I2C1_SCL = 45
    HPD_LV = 46
    EMMC_EN = 47

class RP12A(object):
    """Base functionality for RP12A."""

    PIN = pinout()

    #Initialise SPI
    spi0 = spidev.SpiDev()
    spi0.open(0,0)
    spi0.max_speed_hz = 1000000
    spi0.mode = 0
    spi0.no_cs = True	# Automatic CS off so you can manually control CS

    #Initialise I2Cs
    bus0 = smbus.SMBus(0)
    bus1 = smbus.SMBus(1)

    #Initialise Serials
   # try:
   #     rs485 = serial.Serial("/dev/ttyUSB0", baudrate=115200, timeout=5)
   # except:
   #     rs485 = None
   #     print("rs485 module failed")

    try:
        simcom = serial.Serial("/dev/serial0", baudrate=115200, timeout=1)
    except:
        simcom = None
        print("simcom module failed")
    
    def __init__(self, **kwargs):

        #Initialise SPI chip select pins
        GPIO.setup(self.PIN.SPI0_CS0,GPIO.OUT,initial=1)
        GPIO.setup(self.PIN.SPI0_CS1,GPIO.OUT,initial=1)
        GPIO.setup(self.PIN.SPI0_CS2,GPIO.OUT,initial=1)

        #Initialise spare gpios
        GPIO.setup(self.PIN.SPARE_F0,GPIO.OUT,initial=0)
        GPIO.setup(self.PIN.SPARE_F1,GPIO.OUT,initial=0)
        GPIO.setup(self.PIN.SPARE_F2,GPIO.OUT,initial=0)
        GPIO.setup(self.PIN.SPARE_F3,GPIO.OUT,initial=0)

