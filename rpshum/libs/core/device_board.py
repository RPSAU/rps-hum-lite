from .drivers import ee_rp12a, tca9539, ads1115, tmp1075
import time

class DeviceBoard:
    def __init__(self):
        self.__board__ = ee_rp12a.RP12A()
        self.__gpiox__ = tca9539.TCA9539(self.__board__.bus0)
        self.__ain__ = ads1115.ADS1115(self.__board__.bus0)
        self.__temperature__ = tmp1075.TMP1075(self.__board__.bus0)
        self.isBusy = False
        self.timeDelay = 0.01

    def digital_read(self,reg):
        while (self.isBusy):
            time.sleep(self.timeDelay)
        try:
            self.isBusy = True
            data = self.__gpiox__.read(reg)
        except Exception as e:
            print(e)
            data = None
            pass

        self.isBusy = False
        return data

    def digital_write(self, reg, value):
        while (self.isBusy):
            time.sleep(self.timeDelay)
            
        try:
            self.isBusy = True
            #reg_values = self.digital_read()
            data = self.__gpiox__.write(value,reg=reg)
        except Exception as e:
            print(e)
            data = None
            pass
        
        self.isBusy = False
        return data

    def analog_read(self, reg):
        while (self.isBusy):
            time.sleep(self.timeDelay)
        try:
            self.isBusy = True
            data =  self.__ain__.read_adc(reg)
        except Exception as e:
            print(e)
            data = None
            pass    
            
        self.isBusy = False
        return data

    def analog_write(self, reg, value):
        while (self.isBusy):
            time.sleep(self.timeDelay)
        try:
            self.isBusy = True
            data = self.__ain__.write(value, reg)
        except Exception as e:
            print(e)
            data = None
            
        self.isBusy = False
        return data
        
    def temperature_read(self):
        while (self.isBusy):
            time.sleep(self.timeDelay)
        
        try:
            self.isBusy = True
            data = self.__temperature__.read_temperature()
        except Exception as e:
            print(e)
            data = None
        self.isBusy = False
        return data
