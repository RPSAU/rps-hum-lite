

# the contingent string (time in string)
def checkTimeCondition(timeStart, timeEnd):
    try:
        [hour_start,min_start,sec_start] = parseTimeStr(timeStart)
        [hour_end,min_end,sec_end]  = parseTimeStr(timeEnd)
        
        cur_dt = datetime.datetime.now()
        start_dt = cur_dt.replace(hour=hour_start,minute=min_start,second=sec_start)
        end_dt = cur_dt.replace(hour=hour_end,minute=min_end,second=sec_end)
        
        #if end_dt < start_dt:
        #    end_dt += datetime.timedelta(days=1)
            
        if end_dt < start_dt:
            return False
       
        if cur_dt < start_dt:
            return False
        
        if cur_dt > end_dt:
            return False
       
        return True
    except:
        return False
        
        
# return true if the contingent is scheduled to be active.
def isTimeScheduleActive(contingentConf):
    # only check if "BETWEEN" 
    if conf["timeRange"].upper() == "AT":
        return False
        
    daysofweek = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
    conf = contingentConf
    cur_dt =  datetime.datetime.now()
    cur_day = daysofweek[cur_dt.weekday()]
    
    if "anyTime" not in conf.keys():
        conf["anyTime"] = False
        
    if conf["anyTime"]:
        return True
        
    # not anyTime, check pickDays and time
    if conf["pickDays"][cur_day] == False:
        return False


    elif conf["timeRange"].upper() == "BETWEEN":
        return checkTimeCondition(conf["timeRangeStart"], conf["timeRangeEnd"])
    
    return False
    