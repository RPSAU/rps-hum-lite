from . import modbus_device
import sunspec.core.client as sunspecClient

class SunnyBoy6Device(modbus_device.MobusDevice):
    def __init__(self, id, conf):
        conf['type'] = "SUNNYBOY_ISLAND_6"
        modbus_device.MobusDevice.__init__(self, id, conf)
        self.sunspecClient = None
        self.sp_datapoints = {"common": ["SN"],"inverter":["AphA","PhVphA","WH","Evt1"],"nameplate":["WHRtg"],"status": ["ActWh"],"storage":["InBatV","ChaState"]}
    
    def read(self):
        data = self.__getData()
        return data

    def read_measurement(self, measurement):
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
            
        pass
        
    def write(self, measurement, value):

        if not self.isControllable():
            print("Device is not controllable")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        
        measurement_data = self.conf["measurements"][measurement]
        pass 
	
    def __read_sunspec_data(self):
        
        data = {}
        try:
            #self.sunspecClient = sunspecClient.SunSpecClientDevice(sunspecClient.TCP,self.conf["sunspecConf"]["slaveId"],ipaddr=self.conf["sunspecConf"]["ip"],ipport=self.conf["sunspecConf"]["port"])
            for m, ps in self.sp_datapoints.items():
                self.sunspecClient[m].read()
                for p in ps:
                    data[p] = self.sunspecClient[m][p]
            self.sunspecClient.close()
        except: 
            self.sunspecClient.close()
            pass
        return data

    def __read_modbus_data(self):
        data = {}
        try:
            isConnected = self.client.connect()
            if isConnected:
                #print("Modbus connected")
                # read operation.health
                addr = 30201
                nbyte = 2
                rr = self.client.read_holding_registers(addr,nbyte,unit=MB_SLAVEID)
                decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
                data['Health'] = decoder.decode_32bit_uint()
                self.client.close()
            else:
                print("Modbus counldn't be connected")
        except:
            print("Could not connect the device")
            pass
         
        return data


    def __getData(self):
        data = {}
        try:
            # connect the device
            #self.isConnected = self.client.connect()
            
            # read sunspec data
            result = self.__read_sunspec_data()
            for key,value in result.items():
                data[key] = value
                
            # read modbus data
            result = self.__read_modbus_data()
            for key,value in result.items():
                data[key] = value
        except:
            print("Can not read the data")
        
        return data

