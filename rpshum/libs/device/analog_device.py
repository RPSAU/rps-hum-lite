from . import device


class AnalogDevice(device.Device):
    def __init__(self, id, conf, board = None):
        conf['type'] = "ANALOG"
        device.Device.__init__(self, id, conf)
        self.board = board

    def set_board(self, board):
        self.board = board

    def read(self):
        if self.board is None:
            print("GPIO board not attached")
            return None
        
        data = {}
        for m in self.conf["measurements"]:
            data[m] = round(self.read_measurement(m),2)
            
        return data

    def __scaling(self, value):
        if "deviceModel" in self.conf["humConf"].keys(): 
            if self.conf["humConf"]["deviceModel"] == "200SS-VA":
                return (100 - 100*value/2.8)
            else:
                return value
        
        return value
        
    def read_measurement(self, measurement):
        if self.board is None:
            print("GPIO board not attached")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        
        
        measurement_data = self.conf["measurements"][measurement]
        
        # read measurement 
        data_value = self.board.analog_read(measurement_data["read_channel"])
        data_value = self.__scaling(data_value)
        
        '''
        if "scaling" in measurement_data.keys(){
            data_value = data_value*measurement_data["scaling"]
        }
        '''
        return data_value
        
    def write(self, measurement, value):
        if self.board is None:
            print("GPIO board not attached")
            return None
            
        if not self.isControllable():
            print("Device is not controllable")
            return None

        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        
        measurement_data = self.conf["measurements"][measurement]
        if not measurement_data["isManualOverride"]:
            print("cannot override {}".format(measurement))
            return None	
            
        return self.board.analog_write(measurement_data["write_channel"], value)

    # Execute the schedule schedule.
    # Do not remove the funtion. 
    def runJob(self, cont, devs = None):
        print("Run job scheduled for analog device")
        pass
