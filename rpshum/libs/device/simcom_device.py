# Author: Jacob Hartland
# Company: Elemental Electronics
# Description: Basic library for SIM7600
# Functionality: Provides read and write commands for SIM7600.
# Code has been modified by RPS team. 

import serial
import time
from . import device

class Sim7600saDevice(device.Device):
    """Base functionality for SIM7600SA"""
    def __init__(self, id, conf):
        conf["type"] = "SIM7600SA"
        device.Device.__init__(self, id, conf)
        self.data = ""
        self.uart = self.__initUart()
        self.__setupSIMCOM()
        
        
    def __initUart(self):
        conf = self.conf["serialConf"]
        if "timeout" not in conf.keys():
            conf["timeout"] = 1
         
        return serial.Serial(conf["name"],baudrate=conf["baudrate"], timeout=conf["timeout"])
        
    def __setupSIMCOM(self):

        for i in range(3):
            if self.command('AT','OK',debug=1):
                break
            time.sleep(0.5)
        #self.command('AT','OK',debug=1)
        for i in range(3):
            if self.command('AT+CGPS=1','OK',debug=1):
                break
            time.sleep(0.5)
             
        self.command('AT+CREG?','OK',debug=1)
        time.sleep(2)
    
    def __read(self, size=5096, debug=0):
        data = self.uart.read(size)
        if debug == 1:
            print(data)
        return data

    def __write(self, data):
        data = data + '\r\n'
        self.uart.write(str.encode(data))

    def command(self, cmd, find='OK', debug=0):
        self.data = ""
        #self.uart.baudrate = baudrate
        self.__write(cmd)
        #time.sleep(0.010)
        receive = self.__read()
        if debug == 1:
            print(receive)
        if receive.find(str.encode(find)) > -1:
            #print("success")
            self.data = receive.decode("utf-8")
            return True
        else:
            #print("failed")
            return False
            
    def __parseGPSStr(self,dataStr):
        data = {}
        dataStr = dataStr.strip()
        if len(dataStr) == 0: 
            return ""
        
        strs = dataStr.split(":")
        if len(strs) != 2:
            return ""
       
        temp =  strs[1].split(",")
        if len(temp)<6:
            return ""
        
        # latitude
        latStr = temp[0].strip()
        if latStr == "":
            return ""
        
        data["lat"] = -float(latStr)/100 if temp[1].strip() == "S" else  float(latStr)/100
        
        #data["indLt"] = temp[1].strip()

        logStr = temp[2].strip()
        if logStr == "": 
            return "" 
        
        data["lng"] = float(logStr)/100 if temp[3].strip() == "E" else -float(logStr)/100 
 
        #data["indLg"] = temp[3].strip()

        data["date"] = temp[4].strip()
        data["time_UTC"] = temp[5].strip()

        data["alt"] = temp[6].strip()

        data["spd"] = temp[7].strip() 

        tempcs = temp[8].strip()
        new_tempcs = tempcs[:-6]
 
        data["cs"] = new_tempcs

        return data
        
        
    def readGPSdata(self):
        self.command('AT+CGPSINFO','OK',debug=1)
        return self.__parseGPSStr(self.data)
        
