from . import device


class SimulatedDigitalDevice(device.Device):
    def __init__(self, id, conf):
        conf['type'] = "SIMULATED_DIGITAL"
        device.Device.__init__(self, id, conf)
        self.value = 0
        

    def On(self):
        assert "controlMeasurements" in self.conf.keys(), "Controll measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        self.write(self.conf["controlMeasurements"][0],1)

    def Off(self):
        assert "controlMeasurements" in self.conf.keys(), "Controll measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        self.write(self.conf["controlMeasurements"][0],0)
            

    def runJob(self, onContingent):
        print("Running Job onContingent {}".format(onContingent))
        
    def read(self):
        data = {}
        #print(self.value)
        for m in self.conf["measurements"]:
            data[m] = self.read_measurement(m)
        return data

    def read_measurement(self, measurement):

        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
            
        measurement_data = self.conf["measurements"][measurement]	
        value = self.conf["humConf"]["measurements"][measurement_data["humName"]]["value"]

        return value 
        
    def write(self, measurement, value):

        if not self.isControllable():
            print("Device is not controllable")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        
        measurement_data = self.conf["measurements"][measurement]
        if not measurement_data["isManualOverride"]:
            print("cannot override {}".format(measurement))
            return None	
            
        # read current register value
        print(measurement_data, value)
        self.conf["humConf"]["measurements"][measurement_data["humName"]]["value"] = value
        self.value = value
        #print(self.conf["humConf"])
	
