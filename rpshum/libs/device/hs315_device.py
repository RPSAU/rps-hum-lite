from . import modbus_device
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

class Hs315Device(modbus_device.ModbusDevice):
    def __init__(self, id, conf):
        conf['type'] = "HS315"
        modbus_device.ModbusDevice.__init__(self, id, conf)

    def read(self):
        data = self.__getData()
        return data

    '''
        "fuel_lvl": "", 
        "run_time": "",
        "output_V": "",
        "output_P": "",
        "output_I": "",
        "crank_V": "",
        "err_gen": ""
    '''
    def __getData(self):
        data = {}
        try:
            # connect the device
            self.isConnected = self.client.connect()
            
            # read DC Voltage
            rr = self.client.read_input_registers(1101,2,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['output_V'] = decoder.decode_32bit_int()/256
            
            # read DC Current
            rr = self.client.read_input_registers(1109,2,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['output_I'] = decoder.decode_32bit_int()/256
            
            
            # read DC Power
            rr = self.client.read_input_registers(1117,2,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['output_P'] = decoder.decode_32bit_int()/256
            
            # read generator status
            rr = self.client.read_input_registers(132,1,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['status_engine'] = decoder.decode_16bit_uint()

            # read fuel level
            rr = self.client.read_input_registers(1471,2,unit=0x1)
            #print(rr.registers)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            fuel_lvl = decoder.decode_32bit_int()
            data['fuel_Vdc'] = fuel_lvl/65536
            #data['fuel_lvl'] = None if fuel_lvl>65534 else fuel_lvl/256

            rr = self.client.read_input_registers(1801,2,unit=0x1)
            #print(rr.registers)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            fuel_lvl = decoder.decode_32bit_int()
            data['fuel_level(%)'] = fuel_lvl/256
            
            # read running hours
            # rr = self.client.read_input_registers(71,2,unit=0x1)
            # decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            # data['run_time'] = decoder.decode_32bit_uint()
            rr = self.client.read_holding_registers(9,2,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['run_time'] = decoder.decode_32bit_uint()
            
            rr = self.client.read_holding_registers(25,2,unit=0x1)
            decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            data['run_time_load'] = decoder.decode_32bit_uint()
            
            # rr = self.client.read_input_registers(167,1,unit=0x1)
            # decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
            # data['status'] = decoder.decode_16bit_uint()

        except Exception as e:
            print(e)
            print("Can not read the data")
        
        self.client.close()
        return data

