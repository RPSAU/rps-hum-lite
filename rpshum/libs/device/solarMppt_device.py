from . import device
import serial

class SolarMpptDevice(device.Device):
    def __init__(self, id, conf):
        conf['type'] = "SOLAR_MPPT"
        device.Device.__init__(self, id, conf)
        self.__setupSerial()
        self.packetTypes = ["PID","FW", "SER#","V", "I","VPV","PPV", "CS", "ERR", "LOAD", "IL", "H19", "H20", "H21", "H22", "H23", "HSDS"]
        
    def __setupSerial(self):
        try:
            self.serial = serial.Serial()
            self.serial.baudrate = self.conf["serialConf"]["baudrate"]
            self.serial.port = self.conf["serialConf"]["name"]
            self.serial.parity = self.conf["serialConf"]["parity"]
            self.serial.stopbits = self.conf["serialConf"]["stopbits"]
            self.serial.bytesize = self.conf["serialConf"]["databits"]
            self.serial.timeout = 1
        except Exception as e:
            print(e)
            self.serial = None

    def read(self):
        data = self.__getData()
        return data

    
    def __getPacketValue(self,packetStr):
        pass
        
    def __read_frame(self):
        data = {}
        isDataFrame = False
        while True:
            packet_frame = self.serial.readline().split('\t')
            print(packet_frame)
            if len(packet_frame) != 2:
                continue
                
            # identify the first packet PID
            packetType = packet_frame[0].strip()
            if packetType == "PID":
                isDataFrame = True
            elif not isDataFrame:
                continue
            
            packetValue = self.__getPacketValue(packet_frame[1])
            data[packetType] = packetValue
            
            if len(data) == len(self.packetTypes):
                break
                
        return data  

    def __getData(self):
        data = {}
        try:
            if self.serial is None:
                print("serial port not configured")
                return data

            if self.serial.is_open:
                self.serial.close()
                
            # open the port
            self.serial.open()
            for i in range(10):
                packet_frame = self.serial.readline()
                print(packet_frame)
            
            #self.__read_frame()
            self.serial.close()
        except Exception as e:
            self.serial.close()
            print(e)
        
        
        return data

