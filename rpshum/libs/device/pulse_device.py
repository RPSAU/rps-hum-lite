from . import device
import time


class PulseDevice(device.Device):
    def __init__(self, id, conf, board = None):
        conf['type'] = "PULSE"
        device.Device.__init__(self, id, conf)
        self.board = board
        

    def On(self):
        assert "controlMeasurements" in self.conf.keys(), "Controll measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        self.__sendPulse()
        

    def Off(self):
        assert "controlMeasurements" in self.conf.keys(), "Controll measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        #self.write(self.conf["controlMeasurements"][0],0)
        self.__sendPulse()
    
    # Execute the schedule schedule.
    # Do not remove the funtion. 
    def runJob(self, cont):
        if cont["onContingent"] == True:
            self.On()
        else:
            self.Off()
    
    def __sendPulse(self):
        # send high
        self.write(self.conf["controlMeasurements"][0],1)
        time.sleep(self.conf["humConf"]["pulseDuration"])
        # send low
        self.write(self.conf["controlMeasurements"][0],0)    
        
    def read(self):
        if self.board is None:
            print("GPIO board not attached")
            return None
        data = {}
        for m in self.conf["measurements"]:
            data[m] = self.read_measurement(m)
        return data

    def read_measurement(self, measurement):
        if self.board is None:
            print("GPIO board not attached")
            return None
            
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
            
        measurement_data = self.conf["measurements"][measurement]	
        reg_value = self.board.digital_read(measurement_data["read_reg"])
        #print(reg_value)
        bit_value = (reg_value[0] & (2**measurement_data["port_bit"])) >> measurement_data["port_bit"]
        return bit_value 
        
    def write(self, measurement, value):
        if self.board is None:
            print("GPIO board not attached")
            return None
        if not self.isControllable():
            print("Device is not controllable")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        try:
            measurement_data = self.conf["measurements"][measurement]
            if not measurement_data["isManualOverride"]:
                print("cannot override {}".format(measurement))
                return None	
                
            # read current register value
            cur_value = self.board.digital_read(measurement_data["read_reg"])[0]
            bit_pos = measurement_data["port_bit"] 
            if value == 0:
                cur_value = ~(2**bit_pos) & cur_value 
            elif value == 1:
                cur_value = (2**bit_pos) | cur_value 
            return self.board.digital_write(measurement_data["write_reg"], cur_value)
        except Exception as e:
            print(e)
            return None
