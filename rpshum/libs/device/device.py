import json

class Device:
	def __init__(self, id, conf):
		self.id = id
		self.conf =  conf
		self.name = conf["name"]
		self.type = conf["type"]
		
	def isControllable(self):
		if "controllable" in self.conf.keys():
			return self.conf['controllable']
		else:
			return False
			
	def update():
		return 

	def __str__(self):
		return "{}: {}".format(self.id,self.conf)
