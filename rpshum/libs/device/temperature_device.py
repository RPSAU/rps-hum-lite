from . import device


class TemperatureDevice(device.Device):
    def __init__(self, id, conf, board = None):
        conf['type'] = "TEMPERATURE"
        device.Device.__init__(self, id, conf)
        self.board = board
        
    def read(self):
        data = {}
        if self.board is None:
            print("GPIO board not attached")
            return data
        data = {"Temperature":round(self.board.temperature_read(),2)}
        return data

    def read_measurement(self, measurement):
        print("function unavailble for temperature device")
        return None
        if self.board is None:
            print("GPIO board not attached")
            return None
            
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
            
        measurement_data = self.conf["measurements"][measurement]	
        return self.board.digital_read(measurement_data["read_reg"])
        
    def write(self, measurement, value):
        if self.board is None:
            print("GPIO board not attached")
            return None
        if not self.isControllable():
            print("Device is not controllable")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        
        measurement_data = self.conf["measurements"][measurement]
        if not measurement_data["isManualOverride"]:
            print("cannot override {}".format(measurement))
            return None	
            
        return self.board.digital_write(measurement_data["write_reg"], value)
	
