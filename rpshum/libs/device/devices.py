from . import hs315_device, solarMppt_device, vedirect_device, simcom_device

class Devices:
    def __init__(self):
        # intialize the board
        self.__devicelist__ = {}
        

    def DeviceList(self):
        return self.__devicelist__

    def delete_device(self, id):
        if id in self.__devicelist__.keys():
            del self.__devicelist__[id]

    def update_device(self, id):
        dev = self.__devicelist__[id]
        dev.update()
        #self.__devicelist__[id]["conf"] = 

    def add_device(self, id, devConf):
        dev = None
        
        if devConf["humConf"]["type"] == "HS315":
            dev = hs315_device.Hs315Device(id, devConf)
        elif devConf["humConf"]["type"] == "SOLAR_MPPT":
            dev = vedirect_device.VeDirect(id, devConf)
            #dev = solarMppt_device.SolarMpptDevice(id, devConf)
        elif devConf["humConf"]["type"] == "VE_SHUNT":
            dev = vedirect_device.VeDirect(id, devConf)
        elif devConf["humConf"]["type"] == "SIM7600SA":
            dev = simcom_device.Sim7600saDevice(id, devConf)
            
            
        if dev is not None:
            self.__devicelist__[id] = dev
            setattr(self, id, dev)

    def __getitem__(self, item):
        return getattr(self, item)
