from . import device
from .. import utils
import datetime, time


class DigitalDevice(device.Device):
    def __init__(self, id, conf, board = None):
        conf['type'] = "DIGITAL"
        device.Device.__init__(self, id, conf)
        self.board = board
        

    def On(self):
        assert "controlMeasurements" in self.conf.keys(), "Control measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        self.write(self.conf["controlMeasurements"][0],1)

    def Off(self):
        assert "controlMeasurements" in self.conf.keys(), "Control measurement not found!"
        assert len(self.conf["controlMeasurements"]) > 0 , "Empty controlMeasurements!"
        self.write(self.conf["controlMeasurements"][0],0)
    
    # Execute the schedule .
    # Do not remove the funtion. 
    '''
    def runJob(self, cont, devs = None):
        if not (cont["type"].upper() == "TIME"):
            return 
        
        if cont["timeRange"].upper() == "AT":
            self.__setContingent(cont["onContingent"])
        elif cont["timeRange"].upper() == "BETWEEN":
            print("Running time between job ")
            if self.contingentCondition(cont, devs):
                self.__setContingent(cont["onContingent"])
                if cont["onContingent"] == True:
                    if "holdTime" in cont.keys():
                        if cont["holdTime"] > 0:
                            time.sleep(cont["holdTime"])
                            self.__setContingent(False)
                
    '''        
    def runJob(self, cont, devs = None):
        if not (cont["type"].upper() == "TIME"):
            return 
        
        if cont["timeRange"].upper() == "AT":
            if utils.isTimeValid(cont):
                self.__setContingent(cont["onContingent"])
        elif cont["timeRange"].upper() == "BETWEEN":
            #print("Running time between job ")
 
            # set contingent if not sensedevice dependant
            if "senseDevices" not in cont.keys():
                self.__setContingent(cont["onContingent"])
            elif len(cont["senseDevices"]) == 0:
                self.__setContingent(cont["onContingent"])
            
            while True:
                isTimeValid = utils.isTimeValid(cont)
                if not isTimeValid:
                    break;

                # check device sensing
                if "senseDevices" not in cont.keys():
                    if "senseRate" not in cont.keys():
                        time.sleep(5)
                    else:
                        time.sleep(cont["senseRate"])
                    continue
                
                if len(cont["senseDevices"]) == 0 :
                    if "senseRate" not in cont.keys():
                        time.sleep(5)
                    else:
                        time.sleep(cont["senseRate"])
                    continue
                
                if devs is None:
                    print("No device list for sense contingent")
                    break;
                
   
                #if isSense:
                if self.__checkSenseCondition(cont,devs):
                    # set contingent
                    self.__setContingent(cont["onContingent"])
                    
                    # safe switch off after holdTime if defined.
                    if cont["onContingent"] == True:
                        if "holdTime" in cont.keys():
                            if cont["holdTime"] > 0:
                                time.sleep(cont["holdTime"])
                                self.__setContingent(False)
                else:
                    self.__setContingent(not cont["onContingent"])
                    
                if "senseRate" not in cont.keys():
                    time.sleep(5) 
                else:
                    time.sleep(cont["senseRate"])
            
            #print("Between contingent exit.")
            self.__setContingent(not cont["onContingent"])
            
    def __setContingent(self, onContingent):
        if onContingent == True:
            self.On()
        else:
            self.Off()
    
    def __checkSenseCondition(self, cont, devs):
        for sensecont in cont["senseDevices"]:
            dev = devs[sensecont["deviceId"]]
            val = dev.read_measurement(sensecont["measurement"])
            senseCondtion = utils.checkSenseCondition(val,sensecont)
            print("read {} val {} -> {}".format(sensecont["measurement"],val,senseCondtion))
            if not senseCondtion:
                return False
        return True
        
    def contingentCondition(self, cont, devs):
        daysofweek = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
   
        # check day of the week
        cur_dt =  datetime.datetime.now()
        cur_day = daysofweek[cur_dt.weekday()]
        if cont["pickDays"][cur_day] == False:
            return False
        
        # Time condition has been done when scheduling the job. No need for checking again.
        
        # check sensing device condition
        if len(cont["senseDevices"]) == 0: 
            return True
        
        if devs is None:
            print("No device list for sense contingent")
            return False
                

        for sensecont in cont["senseDevices"]:
            dev = devs[sensecont["deviceId"]]
            val = dev.read_measurement(sensecont["measurement"])
            
            print("read PIR val {}".format(val))
            if not utils.checkSenseCondition(val,sensecont):
                return False
        
        return True

    def read(self):
        if self.board is None:
            print("GPIO board not attached")
            return None
        data = {}
        for m in self.conf["measurements"]:
            data[m] = self.read_measurement(m)
        return data

    def read_measurement(self, measurement):
        if self.board is None:
            print("GPIO board not attached")
            return None
            
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
            
        measurement_data = self.conf["measurements"][measurement]	
        reg_value = self.board.digital_read(measurement_data["read_reg"])
        #print(reg_value)
        bit_value = (reg_value[0] & (2**measurement_data["port_bit"])) >> measurement_data["port_bit"]
        return bit_value 
        
    def write(self, measurement, value):
        if self.board is None:
            print("GPIO board not attached")
            return None
        if not self.isControllable():
            print("Device is not controllable")
            return None
        
        if not (measurement in self.conf["measurements"]):
            print("{} not defined".format(measurement))
            return None
        try:
            measurement_data = self.conf["measurements"][measurement]
            if not measurement_data["isManualOverride"]:
                print("cannot override {}".format(measurement))
                return None	
                
            # read current register value
            cur_value = self.board.digital_read(measurement_data["read_reg"])[0]
            bit_pos = measurement_data["port_bit"] 
            if value == 0:
                cur_value = ~(2**bit_pos) & cur_value 
            elif value == 1:
                cur_value = (2**bit_pos) | cur_value 
            return self.board.digital_write(measurement_data["write_reg"], cur_value)
        except Exception as e:
            print(e)
            return None
