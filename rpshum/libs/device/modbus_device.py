from . import device
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.compat import iteritems


class ModbusDevice(device.Device):
    def __init__(self, id, conf):
        conf['type'] = "MODBUS"
        device.Device.__init__(self, id, conf)
        self.client = None
        self.isConnected = False
        self.__init_mobus()
    
    def __init_mobus(self):
        try: 
            print("init modbus", self.conf["modbusConf"])
            if self.conf["modbusConf"]["type"].upper() == "TCP":
                self.client = ModbusClient(self.conf["modbusConf"]["ip"],self.conf["modbusConf"]["port"])
            elif self.conf["modbusConf"]["type"].upper() == "RTU":
                pass
        except:
            print("Device config error")
            pass
    
    