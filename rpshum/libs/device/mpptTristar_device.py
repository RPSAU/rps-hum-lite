from . import modbus_device

class MpptTriStarDevice(modbus_device.ModbusDevice):
    def __init__(self, id, conf):
        conf['type'] = "MPPT_TRISTAR"
        modbus_device.ModbusDevice.__init__(self, id, conf)

    def read(self):
        data = self.__getData()

        return data

	
    def __read_scaling(self):
        rr = self.client.read_input_registers(0,4,unit=0x1)
        #print(rr.registers)
        return rr.registers[0], rr.registers[2]

    def __read_filter_adc(self):
        rr = self.client.read_input_registers(24,11,unit=0x1)
    
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
        decoded = {
                    'vb_filter': decoder.decode_16bit_int()*v_pu*2**-15,
                    'vb_term_filter': decoder.decode_16bit_int()*v_pu*2**-15,
                    'vb_sensor': decoder.decode_16bit_int()*v_pu*2**-15,
                    'va_filter': decoder.decode_16bit_int()*v_pu*2**-15,
                    'ib_filter': decoder.decode_16bit_int()*i_pu*2**-15,
                    'ia_filter': decoder.decode_16bit_int()*i_pu*2**-15,
                }
        #print("-------- Filter ADC Read -------------")
        #for name, value in iteritems(decoded):
        #    print("%s\t" % name, value)
        return decoded



    def __read_mppt(self):
        rr = self.client.read_input_registers(58,5,unit=0x1)
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
        decoded = {
                    'power_out': decoder.decode_16bit_uint()*v_pu*i_pu*2**-17,
                    'power_in': decoder.decode_16bit_uint()*v_pu*i_pu*2**-17,
                    'sweep_pmax': decoder.decode_16bit_uint()*v_pu*i_pu*2**-17,
                    'sweep_vmp': decoder.decode_16bit_int()*v_pu*2**-15,
                    'sweep_voc': decoder.decode_16bit_int()*v_pu*2**-15,
                }
        #print("-------- MPPT Read --------------")
        #for name, value in iteritems(decoded):
        #    print("%s\t" % name, value)
    
        return decoded
    
    def __read_temperature(self):
        rr = self.client.read_input_registers(35,3,unit=0x1)
    
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
        decoded = {
                    'heatsink': decoder.decode_16bit_int(),
                    'temp_bat': decoder.decode_16bit_int(),
                }
        #print("-------- Temperature Read -------------")
        #for name, value in iteritems(decoded):
        #    print("%s\t" % name, value)
        return decoded

    def __read_charger(self):
        rr = self.client.read_input_registers(50,8,unit=0x1)
    
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)^M
        decoded = {
                    'charge_state': decoder.decode_16bit_uint(),
                    'v_target': decoder.decode_16bit_int()*v_pu*2**-15,
                    'ah_resettable': decoder.decode_32bit_uint()*0.1,
                    'ah_total': decoder.decode_32bit_uint()*0.1,
                    'kwh_resettable': decoder.decode_16bit_uint(),
                }
    
        #print("-------- Charger Read ----------")
        #for name, value in iteritems(decoded):
        #    print("%s\t" % name, value)
        return decoded
    
        
    def __read_status(self):
    
        rr = self.client.read_input_registers(38,12,unit=0x1)
    
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers,Endian.Big)
        decoded = {
                    'v_bat': decoder.decode_16bit_int()*v_pu*2**-15,
                    'a_bat': decoder.decode_16bit_int()*i_pu*2**-15,
                    'vb_min': decoder.decode_16bit_int()*v_pu*2**-15,
                    'vb_max': decoder.decode_16bit_int()*v_pu*2**-15,
                    'hourmeter': decoder.decode_32bit_uint(),
                    'fault_all': decoder.decode_16bit_uint(),
                    'reserve': decoder.decode_16bit_uint(),
                    'alarm': decoder.decode_32bit_uint(),
                }
    
        #print("-------- Status Read -----------")
        #for name, value in iteritems(decoded):
        #    print("%s\t" % name, value)
    
        return decoded
    
    def __getData(self):
        data = {}
        try:
            # connect the device
            self.isConnected = self.client.connect()
            
            # read filter ADC
            result = self.__read_filter_adc()
            data["Battery Voltage"] = result["vb_filter"]
            data["Charge Current"] = result["ib_filter"]
            data["Array Voltage"] = result["va_filter"]
            data["Array Current"] = result["ia_filter"]
        
            # MPPT
            result = self.__read_mppt()
            data["Power Output"] = result["power_out"]
            data["Sweep Pmax"] = result["sweep_pmax"]
            data["Sweep Vmp"] = result["sweep_vmp"]
            data["Sweep Voc"] = result["sweep_voc"]
            
            # temperature
            result = self.__read_temperature()
            data["Heatsink Temperature"] = result["heatsink"]
            data["Battery Temperature"] = result["temp_bat"]
            
            # Charger
            result = self.__read_charger()
            data["Charge State"] = result["charge_state"]
            data["Target Voltage"] = result["v_target"]
            data["Amp Hours"] = result["ah_resettable"]
            data["Kilowatt Hours"] = result["kwh_resettable"]
            
            # Status
            result = self.__read_status()
            data["Faults"] = result["fault_all"]
            data["Alarms"] = result["alarm"]
        except:
            print("Can not read the data")
        
        self.client.close()
        return data

