import datetime
import time


def get_read_func(devId, para=None):
    funcstr = "read_{}".format(devId.strip().replace("-", "_"))

    if para is not None:
        funcstr = "{}_{}".format(funcstr, para)

    return funcstr


def get_control_func(devId, para):
    return "control_{}_{}".format(devId.strip().replace("-", "_"), para.strip())


def getPayload(stationConf, devId, data, error_code=None):
    payload = {
        stationConf["id"]: {
            'serial': stationConf["serial"],
            'timestamp': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S+08:00'),
            'alertMessage': None,
            'location': stationConf['location'],
            devId: {
                'serial': stationConf["devices"][devId]["humConf"]["serial"],
                'token': stationConf["devices"][devId]["token"],
                "values": data,
                "alertMessage": error_code
            }

        }
    }
    return payload


def parseJobId(jobId):
    (devType, devId, contingentId) = jobId.split("_")
    return devType, devId, contingentId


def parseTimeStr(at):
    """ parse time string in format of HH:MM or HH:MM:SS to tuple (HH, MIN, SEC)."""
    ats = at.strip().split(":")

    if len(ats) < 2 and len(ats) > 3:
        return None

    if len(ats) == 2:
        ats.append("00")

    hour = int(ats[0].strip())
    minute = int(ats[1].strip())
    second = int(ats[2].strip())

    return hour, minute, second

    '''
    second = int(ats[-1].strip())

    dt = datetime.datetime.now()
    minute = dt.minute
    if (len(ats)>=2):
            minute = minute if ats[-2].strip()=='' else int(ats[-2].strip())
    hour = dt.hour
    if len(ats)>=3:
            hour = hour if ats[-3].strip()=='' else int(ats[-3].strip())
    return hour, minute, second
    '''


def getTimeStampfromContingent(hour, min, sec):
    return hour*3600 + min*60 + sec


def checkSenseCondition(val, sensecont):
    if val is None:
        return False
    try:
        
        if sensecont["range"].upper() == "GT":
            if val <= float(sensecont["value"]):
                return False
        elif sensecont["range"].upper() == "LT":
            if val >= float(sensecont["value"]):
                return False
        elif sensecont["range"].upper() == "ET":
            if val != float(sensecont["value"]):
                return False
        else:
            return False
    except Exception as e:
        print(e)
        # return False

    return True

# from the contingent string


def getTimeDelta(timeStart, timeEnd):
    [hour_start, min_start, sec_start] = parseTimeStr(timeStart)
    [hour_end, min_end, sec_end] = parseTimeStr(timeEnd)

    cur_dt = datetime.datetime.now()
    start_dt = cur_dt.replace(
        hour=hour_start, minute=min_start, second=sec_start)

    #end_dt = datetime.datetime.now()
    end_dt = cur_dt.replace(hour=hour_end, minute=min_end, second=sec_end)

    if end_dt < start_dt:
        end_dt += datetime.timedelta(days=1)

    return end_dt - start_dt

# from the contingent string

def isTimeValid(cont):
    daysofweek = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
    cur_dt = datetime.datetime.now()
    # check days
    if not cont["anyDay"]:
        cur_day = daysofweek[cur_dt.weekday()]
        if cont["pickDays"][cur_day] == False:
            return False
    # check time of day
    if cont["timeRange"].upper() == "BETWEEN":
        if not cont["anyTime"]:
            if not checkTimeCondition(cont["timeRangeStart"], cont["timeRangeEnd"]):
                return False
        
    return True

def checkTimeCondition(timeStart, timeEnd):
    try:
        [hour_start, min_start, sec_start] = parseTimeStr(timeStart)
        [hour_end, min_end, sec_end] = parseTimeStr(timeEnd)

        cur_dt = datetime.datetime.now()
        start_dt = cur_dt.replace(
            hour=hour_start, minute=min_start, second=sec_start)
        end_dt = cur_dt.replace(hour=hour_end, minute=min_end, second=sec_end)

        # if end_dt < start_dt:
        #    end_dt += datetime.timedelta(days=1)

        if end_dt < start_dt:
            return False

        if cur_dt < start_dt:
            return False

        if cur_dt > end_dt:
            return False

        return True
    except Exception as e:
        print(e)
        return False
